---
title:       "Localizing Software for Multicultural Environments"
date:        2012-05-10
changed:     2012-05-30
aliases:     [ /2012/node/34 ]

---
<strong>Speaker:</strong> Runa Bhattacharjee


Translating software interfaces has challenges that are much different than what is seen in the traditional translation industry. The challenges are compounded when geographical and linguistic boundaries do not coincide.  One language may span several geographic areas. Multiple languages in close geographic proximity may share cultural references. Unlike localizing for languages which are largely concentrated in one geography, localizing software interfaces requires special attention to cultural ambiguities, shared references, demographic specialities and other factors.

While there are advantages to being able to reuse localized content, wide disparities often nullify the advantages and leave gaping holes to be laboriously filled. This talk focuses on the variety of multicultural localization environments, their special features, the areas of focus and special needs of potential user groups. The talk will also include a comparison to mostly mono-geographical localization environments and possible areas of collaboration.

The talk is intended for various groups who contribute at the different stages of the localization workflow and will highlight their perspectives. This includes:
<ul>
<li>translators, irrespective of whether they have first hand experience dealing with the multicultural localization scenario</li>
<li>developers who create the source content, and</li>
<li>users of localized software.</li>
</ul> 
The user perspective is often missed in translation activities or when source code is built. New and prospective developers and translators will gain an understanding of the different practices used while localizing software and potential corner cases.
<p>&nbsp; </p>
<h2>Runa Bhattacharjee</h2>
For the past 10 years, Runa Bhattacharjee has been translating and working on localizing numerous Open Source projects, ranging from Desktop interfaces to Operating System tools and lots of things in between. She strongly believes that upstream repositories are the best places to submit changes. At Red Hat in India, she holds a professional portfolio, specializing in Localization. Runa translates and maintains translations for Bengali (Indian version), and is always happy to help anyone wanting to get started with localization. She is one of the contributing authors of the <a href="http://open-advice.org/">Open Advice book</a>. Runa can be reached at runab at fedoraproject dot org or on FreeNode IRC as arrbee.
