---
title:       "Domain Specific Debugging Tools"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/25 ]

---
<strong>Speaker:</strong> Volker Krause

Using (or creating) more and more abstract frameworks allows us to implement complex tasks quickly and efficiently. However, debugging code using such frameworks with conventional instruction-level debuggers can be cumbersome, making it easy to get lost in implementation details of the framework. Domain specific debugging tools that understand the abstraction level help keep the overview.


Debugging a QAbstractItemModel with GDB or following the chain of events in an application using asynchronous KJob's demonstrates the very low-level view that instruction-level debuggers provide. Faced with this, developers often fall back to other approaches, such as classical printf debugging. While simple and effective, this requires code modification, re-compiling, and worse, the same thing every time the same problem appears. Instead, domain specific debuggers allow developers to avoid this manual instrumentation while providing more powerful methods for visualization and analysis than stderr usually can.

This talk will present a number of higher-level debugging tools already available for Qt and KDE frameworks, starting from simple QObject introspection up to monitoring activity in complex multi-process systems such as Akonadi. We will look at ways to create debugging utilities for your own frameworks, using the Free Software tool <a href="https://github.com/KDAB/GammaRay">GammaRay</a>. GammaRay provides a platform-independent way to attach to any Qt application and access its internal object structures at runtime, without requiring any modifications of code. It is extensible by plug-ins. With all the gory details of code injection already implemented, it provides a powerful platform for your own domain specific debuggers for Qt or KDE Frameworks.
<p>&nbsp; </p>
<h2>Volker Krause</h2>
Volker started contributing to KDE back in 2002 and quickly got stuck in the KDE PIM code. Having worked on KDE PIM for some time made him join the newly founded Akonadi project in 2006 where he maintains parts of the core components. In 2010 he started the GammaRay project to address the need for QML debugging tools (which did not yet exist) during the Kontact Touch project. Volker works as a software engineer, consultant and trainer at KDAB.
