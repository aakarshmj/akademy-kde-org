---
title:       "Making Multilingual Wikis a Reality"
date:        2012-05-10
changed:     2012-06-23
aliases:     [ /2012/node/35 ]

---
<strong>Speakers:</strong> Niklas Laxstöm and Claus Christensen

In order to reach the greatest number of users, people from the KDE Community recognize that there is a need for as much information as possible to be available in as many languages as possible. While static texts, such as documentation or how-tos, have been translated by teams successfully, a wiki translation is inherently more complex as the contents can change frequently. 


In the past, individual efforts were started, but their lifespan was short as translators were overwhelmed with difficulties: a problem pertaining not specifically to KDE but also to other communities. To improve this situation, Niklas Laxström started an effort to develop a Mediawiki extension able to handle translations coupled with change tracking. 

This talk considers the difficulties that led to the downfall of earlier wiki translation efforts, the considerations that led to the Translate extension as we know it, and its application and current and potential future use for KDE documentation.

Anne Wilson helped develop the content for this presentation and has been a champion of KDE Userbase since its founding.

<p>&nbsp; </p>
<h2>Niklas Laxstöm, Anne Wilson, Claus Christensen</h2>
Niklas Laxström is a software and language engineer, with an M.A. in Language Technology. He has been a free software contributor since about 2003. Niklas joined Wikipedia in 2004 and soon became a MediaWiki translator. To aid his translation work, he created a wiki which has grown to become translatewiki.net, which now manages translation of dozens of free software projects in hundreds of languages and has thousands of translators. In 2011 he started working for Wikimedia Foundation as a member of the Localization team, where he has helped to deploy web fonts and JavaScript-based input methods to overcome operating system limitations in language support.

Claus Christensen is a physics and maths teacher with an interest in computing and in particular Open Source. He discovered Linux and KDE in 2005. Claus began to translate the UserBase wiki to Danish in 2009, and has since expanded his involvement to cover other aspects of UserBase and the web team. His main interest remains translation and the technologies relating to this.

Anne Wilson became besotted with computing in 1981, and moved her work almost entirely to Linux in 2000. In 1999 she was proud to win her M.Sc. in Information Systems, yet found that community work for KDE—developed particularly over the years from 2004—gave the greatest satisfaction. As with many contributors, it grew out of "scratching her own itch", so long time user support with Kontact was the main focus. In 2008 she became one of the founding members of the UserBase wiki, http://userbase.kde.org.
