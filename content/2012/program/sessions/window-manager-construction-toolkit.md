---
title:       "The Window Manager Construction Toolkit"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/44 ]

---
<strong>Speaker:</strong> Martin Gräßlin

With the release of the KDE Plasma Workspaces 4.8, the Compositor and Window Manager KWin received the first QML integration. Based on the experience of this release, one of the major development targets of the 4.9 release cycle has been based on adding dynamic features to KWin.
 

The window manager scripting API received a complete rewrite; KWin compositing effects can now be written with QtScript. For all scriptable components, KWin uses the Plasma Package structure to give developers one common way to package extensions for the KDE Plasma Workspaces.
 
Lots of work has been invested in the QML bindings. The window decoration theme engine has been rewritten with QML providing additional generic QML bindings. The core of KWin has been prepared so that fullscreen effects like Desktop Grid/Present Windows can be reimplemented with QML. The goal is to have all UI elements of the Compositor written in QML.
 
In this talk we will present new functionality added to KWin that is so powerful that it became a Window Manager Construction Toolkit allowing people to develop their own custom window manager with KWin as the core. We will have a look at how this functionality got added with no new maintenance costs for the development team. We will show how these new scripting bindings improved overall code quality, and why this work is an important prerequisite for porting KWin from X11 to Wayland.
<p>&nbsp; </p>
<h2>Martin Gräßlin</h2>
Martin Gräßlin started contributing to the KDE Compositor and Window Manager KWin around the 4.0 release. After first focusing on effects like desktop cube during a GSoC, he started to develop at the core of the window manager and took over maintainer leadership in late 2010.
