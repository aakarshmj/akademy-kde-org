---
title:       "Tine 2.0 and KDE - An Integration Initiative"
date:        2012-06-25
aliases:     [ /2012/node/56 ]

---
<strong>Speaker:</strong> Cornelius Weiss

Tine 2.0 is a usability focused, real open source web groupware with connectivity for desktop clients per *DAV, JSON-RPC and ActiveSync. In the jungle of business software products which call themself "open", Tine 2.0 stands out by being a real open source project disapproving neo-proprietary approaches like open-core and other baitfish models.


Around the same time when KDE4 development began, Tine 2.0 started from ground up building a new software strongly committed to usability. While usability is sometimes mixed up with oversimplification, Tine 2.0 aims to be "Easy-To-Learn" and "Powerful".

With the goal of bringing together the KDE and Tine 2.0 projects and communities, this talk introduces Tine 2.0, its target audience and the user centric development process. Potential fields of operations will be pointed out where KDE and Tine 2.0 could work together to let the combination of Tine 2.0 and KDE become a great user experience.

<p>&nbsp; </p>
<h2>Cornelius Weiss</h2>
Cornelius Weiss is head of development business solutions at Metaways Infosystems GmbH in Hamburg. During his studies of physics with specialization in quantum computation, he started contributing to several open source projects. From 2004 to 2007 he was core developer of eGroupWare, the predecessor of Tine 2.0. In 2008 he started the development of Tine 2.0 at Metaways, the main sponsor of the project.
