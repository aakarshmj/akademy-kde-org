---
title:       "The future of packaging KDE SC Apps on Windows"
date:        2012-05-10
changed:     2012-06-01
aliases:     [ /2012/node/42 ]

---
<strong>Speakers:</strong> Patrick Spendrin and Patrick von Reth


The focus for KDE on Windows has always been on more than one point. The first is the pure code work to make applications build and run on Windows at all. The building part has mostly been done; only a long list of bugs is left. The second big problem is managing and distributing packages for all applications. We provide a package manager system (kdewin-installer) to install most applications. But as KDE on Windows becomes more stable, users want to have self-contained installers. This presentation is an overview of how these can be created and their advantages and disadvantages. The examples we will show will give developers an idea of the possibilities for getting applications to the Windows user base.

In the near future, the Windows platform will benefit from the work that is currently done in Qt5 and KDE Frameworks 5 by reducing the size of dependencies, but it will also create some work by increasing the number of dependencies, since each framework will be on its own. We will show what will change for installers, how developers can use the transition to reduce their dependencies and decrease the size of the respective installers.

The final point of the KDE on Windows presentation is system integration. There will be a brief overview of what users expect and how developers can achieve that.
<p>&nbsp; </p>
<h2>Patrick Spendrin, Patrick von Reth</h2>
Patrick Spendrin (SaroEngels) maintains a lot of the packages for KDE on Windows, fixes bugs from time to time and tries to get as many people as possible to try out KDE on Windows. He works for KDAB full-time again after he took some time off to take care of his first son.

Patrick von Reth (TheOneRing)
He joined the kde-windows team 2 years ago. He works on the (kde-windows-)emerge tool, Amarok and the IRC client quassel, providing kde-windows builds and installers for amarok and quassel. If his IRC client reports he is offline you may find him running through the woods.
