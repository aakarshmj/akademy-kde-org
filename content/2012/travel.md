---
title:       "Travel"
date:        2012-01-31
changed:     2012-02-03
menu:
  "2012":
    parent: travel
    weight: 3
---
## By Plane

Tallinn's <a href="https://www.tallinn-airport.ee/en/">airport</a> can be reached by direct flights from many major European airports, including Amsterdam, Copenhagen, Frankfurt, Helsinki, London, Moscow, Munich, Oslo, Prague, Stockholm, Riga, Vilnius, and Warsaw or via connecting flights to these airports.

To get to Tallinn from the airport, you can take a bus. Details can be found <a href="https://www.tallinn-airport.ee/en/transport/leaving-the-airport/">here</a>.

Bus service no. 90K runs between the Lennart Meri Tallinn Airport and centrally located Tallinn’s hotels from 8am until 6pm every 30 min throughout the week. A one way ticket costs €2. The bus schedule along with city map can be downloaded <a href="http://www.tourism.tallinn.ee/static/files/021/tallinn_airport_city_centre_bus_90k.pdf">here (large PDF)</a>.

## Getting around in the city

You can get around the city with buses, trolleys and trams that operate regularly from 6:00 to 23:00. Information about lines and timetables is available from the <a href="https://transport.tallinn.ee/#tallinna-linn/en">City of Tallinn web site</a>.

Make sure that you either have a valid ticket before getting on public transportation or that you buy one from the driver immediately upon entering. If you need to buy a ticket, enter at the front door; tickets are only sold at stops, not while the vehicle is moving. Note that tickets bought from the driver are more expensive than those bought at a newsstand.

Bus, trolley and tram tickets can be purchased at newsstands for €1 or from the driver for €1.60. You can also buy a 10 ticket package for €8.

Punch your ticket right after entering at a machine located throughout the bus, trolley or tram. Note that you must punch your ticket immediately upon entering. Passengers travelling with false tickets or without tickets will be fined.

Tallinn public transport also offers 1, 3 and 5-day tickets. These tickets can be bought only at newsstands and not from the driver. A day ticket (24 h) costs €4, a 3-day (72 h) ticket costs €6 and a 5-day (120 h) ticket costs €7. Electronic devices for registering these tickets are situated at the first and second door of the vehicle and will print the time and the date of validity on the ticket.
