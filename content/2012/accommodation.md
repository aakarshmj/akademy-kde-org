---
title:       "Accommodation"
date:        2012-01-31
changed:     2012-06-25
menu:
  "2012":
    parent: travel
    weight: 2
scssFiles:
- /scss/2012/accommodation.scss
---
<p>On this page you will find information on hotels and hostels recommended by Akademy 2012 organizers. July is a busy month in Tallinn. A limited number of inexpensive rooms have been reserved at Academic Hostel and Teko Hostel. <b>We encourage you to plan your trip to Akademy 2012 and book your hotel as soon as possible.</b> We cannot guarantee availability or price.</p>


<strong><a href="http://maps.google.com/maps/ms?ie=UTF&msa=0&msid=216980453853222755928.0004c24b57222a6f5bccc">Map</a> of listed hotels, trolleybus/bus stops and the Akademy 2012 venue—the Estonian Information Technology College to get an idea about the distances. Information about prebooked rooms is available below.</strong>

To get to <strong>Rock Cafe</strong> take the preferred transportation that has one of the following destinations:
<ul>
  <li>Autobussijaam</li>
  <li>Lubja</li>
  <li>Sossimägi</li>
</ul>
For example <a href="http://soiduplaan.tallinn.ee/#plan/02801-1,02802-1/11607-1/map,,,1/en"><strong>this is the way</strong></a> from venue to Rock Cafe. Just change the <strong>departure stop</strong> to your preferred.

<h2><b>PRE-BOOKED HOTELS</b></h2>

We have pre-booked some hotels that offer a bit more comfort :)

You can choose yourself a hotel from our pre-booked range<a href=http://www.conference-expert.eu/en/conferences/s2/165-kde-academy-2012> here.</a>

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Teko Hostel</summary>
    <div class="p-2">
      Teko Hostel is <a href="http://maps.google.com/maps/place?q=teko+hostel&hl=et&cid=1688047662858011534"> located in Tallinn city center</a>, 15 minutes walking distance from the Old Town along Tartu Road, near the Central Market.
    </div>
  </details>

  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/21206-3/02802-1/map,,,1/en" title="public transport route" />~35 min from venue</a></li>
    <li>Single bed: 20 EUR</li>
    <li>Breakfast included</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Metropol Hotel</summary>
    <div class="p-2">
      <a href="http://www.metropol.ee/index.php?lang=eng">Metropol hotel</a> is located in the heart of Tallinn in a recently renovated Rotermanni section, just a walking distance away from the historical old town, main sights, shopping centers and port.
    </div>
  </details>

  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/21206-3/02802-1/map,,,1/en" title="public transport route">~30 min from venue</a></li>
    <li>Single room: 49 EUR</li>
    <li>Double/twin room: 56 EUR</li>
    <li>Breakfast included</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Reval Park Hotel and Casino</summary>
    <div class="p-2">
      <a href="http://www.parkhotel.ee/en"> Reval Park Hotel & Casino</a> is located directly by the Police Park. It offers air-conditioned rooms with cable TV, a minibar and a seating area. It also provides free private parking.
    </div>
  </details>

  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/21206-3/02802-1/map,,,1/en" title="public transport route">~35 min from venue</a>
    </li>
    <li>Single room: 53 EUR</li>
    <li>Double/twin room: 59 EUR</li>
    <li>Breakfast included</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Hotel St. Barbara</summary>
    <div class="p-2">
      <a href="https://stbarbara.ee/en/"> Hotel St. Barbara</a> is centrally located within the walking distance of Tallinn Old Town, sightseeings and of course shopping centres. It has a reputation of a business hotel but is an excellent choice for your family stay as well.
    </div>
  </details>

  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/12002-2/02801-1,02802-1/map,,,1/en" title="public transport route">~25 min from venue</a></li>
    <li>Single room: 56 EUR</li>
    <li>Double/twin room: 62 EUR</li>
    <li>Breakfast included</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Tähetorni Hotel</summary>
    <div class="p-2">
      <a href="https://www.thotell.ee/home.html">Tähetorni hotel</a> is <a href="http://maps.google.com/maps/place?q=T%C3%84HETORNI+HOTELL,+T%C3%A4hetorni+16,+Tallinn,+Eesti&hl=et&ie=UTF8&cid=15244024513567383470">located in a wooded area</a> on the outskirts of Tallinn near the Nõmme district. Tähetorni Hotel offers air conditioned rooms with cable TV and free Wi-Fi in public areas. It also has a sauna.
    </div>
  </details>

  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/03801-1/02801-1,02802-1/map,,,1/en" title="public transport route" />~20 min from venue</a></li>
    <li>Single room: 51 EUR</li>
    <li>Double/twin room: 64 EUR</li>
    <li>Breakfast included</li>
    <li>50% pre-payment</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Center Hotel</summary>
    <div class="p-2">
      <a href="https://centerhotel.ee/?lang=en">Center Hotel</a> is <a href="http://maps.google.com/maps/place?q=Narva+mnt+24+%2F+F.R.+Kreutzwaldi+2,+10120+Tallinn&cid=13710122086866176025">located in Tallinn city center</a>. It offers 55 contemporary guestrooms with nice views of the city. Guestrooms can be booked in different categories and feature different amenities such as complimentary internet, flat-screen TV and fridge. Most of the rooms have en-suite bathrooms. It is also possible to reserve a cheaper room-type with a shared bathroom on the same floor. All the rooms in the hotel are non-smoking.
    </div>
  </details>

  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/10703-1/02802-1/map,,,1/en" title="public transport route" />~35 min from venue</a></li>
    <li>Single room: 40 EUR</li>
    <li>Standard twin room: 45 EUR</li>
    <li>Breakfast buffet included</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Go Hotel Shnelli</summary>
    <div class="p-2">
      <a href="http://www.gohotels.ee/">Go Hotel Shnelli</a> is a new hotel with friendly and personal service. The hotel is <a href="http://maps.google.com/maps/place?q=Go+Hotel+Shnelli,+Toompuiestee+37,+Kesklinn,+Eesti&hl=et&cid=3486436889721296924">located just next to the medieval Old Town of Tallinn</a> and opposite Snelli Park. The city center is nearby. The hotel provides 124 well-furnished guest rooms with a choice of two views - overlooking the medieval Old Town and lovely Snelli Pond or the historic railway.
    </div>
  </details>

  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/21103-1/02802-1/map,,,1/en" title="public transport route" />~30 min from venue</a></li>
    <li>Single room: 42 EUR</li>
    <li>Double/twin room: 48 EUR</li>
    <li>Breakfast buffet included</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">City Hotel Portus</summary>
    <div class="p-2">
      <a href="http://www.tallinnhotels.ee/city-hotel-portus-tallinn" data-proofer-ignore>City Hotel Portus</a> offers reasonable prices and <a href="http://maps.google.com/maps/place?q=city+hotel+portus&cid=6852863089772092417">proximity to Tallinn’s Old Town</a>. The cozy nature of the hotel makes it a good place for relaxing and ideal for an enjoyable stay in Tallinn.
    </div>
  </details>

  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/10701-5/02802-1/map,,,1/en" title="public transport route" />~35 min from venue</a></li>
    <li>Single room: 34 EUR</li>
    <li>Double/twin room: 37 EUR</li>
    <li>Breakfast included</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Hotell Oru</summary>
    <div class="p-2">
      <a href="http://maps.google.com/maps/place?cid=5624131509625924689">Situated by Park Kadriorg</a>, the small, charming, elegant <a href="http://www.oruhotel.ee/">Hotell Oru</a> is close to the sea and in the immediate vicinity of the Song Festival Grounds, a place cherished by Estonians. The Old Town of Tallinn as well as business and trade centers are just a 10-minute drive away.
      <br>
      Hotel Oru has 50 comfortable rooms, including a suite with a sauna, two apartment-style rooms, rooms with a sauna and a Jacuzzi, and family rooms. Each room has a phone, cable TV and a bathroom with a lavatory and a bath or a shower. Special hypoallergenic rooms and rooms for disabled guests. The entire hotel has WiFi coverage.
    </div>
  </details>

  <b>Pricing for summer is not fixed yet, prices below apply until 15th April:</b>
  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/12801-1/02802-1/map,,,1/en" title="public transport route" />~50 min from venue</a></li>
    <li>Single room: 50 EUR</li>
    <li>Double room: 60 EUR</li>
    <li>Breakfast included</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Sokos Hotel Viru</summary>
    <div class="p-2">
      <a href="http://www.sokoshotels.fi/en/hotels/tallinn/">Sokos Hotel Viru</a> is located in <a href="http://maps.google.com/maps/place?cid=611058411639653351"> the center of Tallinn</a>. It offers a wide variety of services for business and leisure travellers. The hotel has a modern meeting center, the stylish restaurant Merineitsi and the cozy restaurant Amarillo. Night Club Cafe Amigo, karaoke and dancing bar Bogart, the saunas and the beauty salon cater to all tastes. WiFi covers all public spaces and guest rooms of the hotel. There is a wealth of sights, shopping opportunities and leisure activities in the close vicinity of the hotel.
    </div>
  </details>

  <b>Prices for 29 June - 02 July 2012:</b>
  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/21206-3/02802-1/map,,,1/en" title="public transport route" />~30 min from venue</a>
    </li>
    <li>Single room: 78 EUR</li>
    <li>Double room: 82 EUR</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Kreutzwald Hotel Tallinn</summary>
    <div class="p-2">
      The <a href="http://www.kreutzwaldhotel.com/">Kreutzwald Hotel Tallinn</a> is <a href="http://maps.google.com/maps/place?cid=14705523023009309316">located close to the National Library of Estonia</a> at Tõnismägi, one of the most famous landmarks of Tallinn, and gets its name from the former name of the library, the Friedrich Reinhold Kreutzwald State Library.
    </div>
  </details>

  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/11102-1/02802-1/map,,,1/en" title="public transport route" />~20 min from venue</a></li>
    <li>Single room: 120 EUR</li>
    <li>Double/twin room: 130 EUR</li>
    <li>Breakfast buffet included</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Hotel Ülemiste</summary>
    <div class="p-2">
      <a href="http://www.ylemistehotel.ee/">Ülemiste Hotel</a> has 70 comfortably furnished standard rooms. It's <a href="http://maps.google.com/maps/place?cid=3425070361287792366">located next to Ülemiste Lake</a> and Ülemiste shopping center. All rooms in the hotel are equipped with shower or bath, air-conditioning, hair dryer, satellite TV, radio, direct-dial telephone, minibar and Internet. There are special rooms for the handicapped, people with allergies and smokers. Smoking is prohibited in non-designated rooms in Tallinn hotels.
    </div>
  </details>

  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/13009-1/02802-1/map,,,1/en" title="public transport route" />~45 min from venue</a></li>
    <li>Single standard room: 53 EUR</li>
    <li>Double/Twin standard room: 59 EUR</li>
    <li>Breakfast and sauna included</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Pirita TOP SPA Hotel</summary>
    <div class="p-2">
      <a href="http://hotels.tallink.com/ee/mainMenu/piritaTopSpa/" data-proofer-ignore>Pirita TOP SPA Hotel</a> offers a choice of health, beauty and wellness services and packages. The hotel is <a href="http://maps.google.com/maps/place?cid=9669843243817619828">located on the seaside</a>, 10 minutes from the center of Tallinn, by the mouth of the Pirita River and close to the beautiful Pirita beach. The three story Pirita TOP SPA Hotel has a total of 267 rooms.
    </div>
  </details>

  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/17907-1/02802-1/map,,,1/en" title="public transport route" />~50 min from venue</a></li>
    <li>Standard room: 80 EUR</li>
    <li>Breakfast, swimming and sauna included</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Rocca al Mare Hotel</summary>
    <div class="p-2">
      <a href="http://www.roccahotels.ee">Rocca al Mare Hotel</a> is <a href="http://maps.google.com/maps/place?q=rocca+al+mare+hotell&hl=et&cid=3348736370176730778"> located at the seashore</a> and it has 35 modern, well equipped rooms, including 2 Junior Suites, 2 Suites and a Superior Suite. All standard rooms have mini-bar, shower, air conditioning, tea and coffee making facilities, wired Internet and Satellite TV. All suites are also equipped with trouser press, safebox, morning coats.
    </div>
  </details>

  <ul class="info">
    <li><a href="http://soiduplaan.tallinn.ee/#plan/00702-1/02802-1/map,,,1/en" title="public transport route" />~35 min from venue</a></li>
    <li>Twin/double room: 85 EUR</li>
    <li>Breakfast, sauna and swimming included</li>
    <li>Payment 2 months in advance</li>
  </ul>
</div><!--//end of item-->

<div class="item">
  <details class="mb-3">
    <summary class="p-2 h5">Academic Hostel</summary>
    <div class="p-2">
      <a href="https://www.academichostel.com/en/">Academic Hostel</a> is located next to <a href="http://maps.google.com/maps/place?q=Academic+Hostel,+Akadeemia+tee,+Mustam%C3%A4e,+Eesti&hl=et&ie=UTF8&cid=14033992616371423640">the Estonian Information Technology College</a>. It offers budget accommodation for people who wish to enjoy an inexpensive but comfortable stay in Tallinn.
      The rooms in the Academic hostel have small kitchen so this hostel is ideal for all of you who would like to be able to cook.
    </div>
  </details>

  <ul class="info">
    <li>~5 min walk from venue</li>
    <li>Double/twin room: 20 EUR</li>
  </ul>

  <b>50 twin rooms (100 beds) are pre-booked from 29 June to 04 July 2012</b>.

  This is only suitable for people not staying the whole event or who don't mind moving part way through the week. In order to book a bed in Academic Hostel send an e-mail containing a password "Akademy2012" to info@academichostel.com stating on what dates and how many beds do you want to book.
  <br>
  Because of the limited amount of cheap accommodation You are advised not to book a room with two beds only for one person.
</div><!--//end of item-->
