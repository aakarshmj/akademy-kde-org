---
title:       "Akademy 2012"
date:        2012-01-30
changed:     2012-06-19
---
**30th June - 6th July 2012, Tallinn, Estonia**

Akademy is the annual world summit of KDE, one of the largest Free Software communities in the world. It is a free, non-commercial event organized by the KDE Community. This year marks 15 years of KDE AND the 10th edition of the KDE Community Summit. **Don't miss out on the celebration!**

Akademy features a 2-day conference with presentations on the latest KDE developments, followed by 5 days of workshops, birds of a feather (BoF) and coding sessions. If you want to meet key KDE contributors, learn about the latest features and enjoy the great atmosphere of this event, join us in Tallinn! You will meet developers, artists, translators, upstream and downstream maintainers, users and FLOSS industry leaders from all over the world.

Akademy 2012 will take place at the [Estonian IT College](http://www.itcollege.ee/en/it-college/).

<!--more-->

<p>At Akademy you can:
<ul>
<li>learn about the latest cool KDE features</li>
<li>gain in-depth technical insights</li>
<li>share knowledge</li>
<li>collaborate with people from other Free Software projects</li>
<li>meet like-minded Free Software enthusiasts and make new friends</li>
<li>take part in deciding the future direction of KDE</li>
<li>dig into code and get work done in BoF sessions</li>
<li>find your next job or your next employee</li>
<li>help promote Free Software</li>
<li>have fun</li>
</ul>
and much more!</p>

<h2>Conference attendee policy</h2>
We want Akademy 2012 to be an enjoyable event for all attendees. Please follow the <a href="/2012/akademy-attendee-policy">attendee policy</a> and help make this a wonderful event.
