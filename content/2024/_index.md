---
title: Akademy 2024
layout: home
menu:
  "2024":
    name: 2024
    weight: 1
  main:
    name: "2024"
    weight: 976
hideSponsors: true
---

We are looking forward to having you join us at Akademy in 2024! We will announce the location and dates soon. For now you can [subscribe to our mailing list](https://mail.kde.org/mailman/listinfo/akademy-attendees) to get notified when we announce them.
