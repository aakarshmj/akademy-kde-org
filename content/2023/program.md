---
title: Program Overview
menu:
  "2023":
    parent: program
    weight: 1
scssFiles:
- /scss/tables.scss
---

<table class="stretch">
  <tr>
    <th>Day</th>
    <th>Morning</th>
    <th>Afternoon</th>
    <th>Evening</th>
  </tr>
  <tr>
    <td>Fri 14th</td>
    <td colspan=2></td>
    <td class="highlight text-center"><a href="https://akademy.kde.org/2023/welcomeevent">Welcome Event</a></td>
  </tr>
  <tr>
    <td>Sat 15th</td>
    <td colspan=2 class="highlight text-center"><a href="https://conf.kde.org/event/5/timetable/?layout=room#20230715">Talks Day 1</a></td>
    <td></td>
  </tr>
  <tr>
    <td>Sun 16th</td>
    <td colspan=2 class="highlight text-center"><a href="https://conf.kde.org/event/5/timetable/?layout=room#20230716">Talks Day 2</a></td>
    <td class="highlight text-center"><a href="https://akademy.kde.org/2023/socialevent">Social Event</a></td>
  </tr>
  <tr>
    <td>Mon 17th</td>
    <td colspan=2 class="highlight text-center"><a href="https://community.kde.org/Akademy/2023/Monday">BoFs, Workshops & Meetings</a><br /><a href="https://ev.kde.org/generalassembly/">KDE eV AGM</a> (1100-1400)</td>
    <td></td>
  </tr>
  <tr>
    <td>Tue 18th</td>
    <td class="highlight text-center"><a href="https://community.kde.org/Akademy/2023/Tuesday">BoFs, Workshops & Meetings</a></td>
    <td class="highlight text-center"><a href="https://community.kde.org/Akademy/2023/Tuesday">BoFs, Workshops & Meetings</a><br /><a href="https://conf.kde.org/event/5/registrations/23/">LGBTQ+ Picnic Lunch</a></td>
    <td></td>
  </tr>
  <tr>
    <td>Wed 19th</td>
    <td colspan=3 class="highlight text-center"><a href="https://akademy.kde.org/2023/daytrip">Daytrip</a></td>
  </tr>
  <tr>
    <td>Thu 20th</td>
    <td colspan=2 class="highlight text-center"><a href="https://conf.kde.org/event/5/timetable/?layout=room#20230720">Trainings</a> and <a href="https://community.kde.org/Akademy/2023/Thursday">BoFs, Workshops & Meetings</a></td>
    <td></td>
  </tr>
  <tr>
    <td>Fri 21st</td>
    <td colspan=2 class="highlight text-center"><a href="https://community.kde.org/Akademy/2023/Friday">BoFs, Workshops & Meetings</a></td>
    <td></td>
  </tr>
</table>
