---
title: Akademy 2023 T-shirts
menu:
  "2023":
    parent: details
    weight: 3
---

Following on from previous years we have the option for buying an official Akademy 2023 T-shirt. As Akademy is hybrid we have two options depending on if you are attending in person or online.

{{< img href="/media/2023/akademy2023-mock-attendees-med.png" src="/media/2023/akademy2023-mock-attendees-wee.png" caption="Mockup of Akademy 2023 T-shirt by Jens Reuterberg" figure_class="text-center" >}}

This year's T-shirts are the same model as last years. We offer them in both a unisex and fitted style. The approximate sizes of the T-shirts are:

#### Unisex

[![](/media/2023/unisex-sizes.png)](/media/2023/unisex-sizes.png)

*4XL and 5XL are not available in this colour unfortunately.*

#### Fitted

[![](/media/2023/fitted-sizes.png)](/media/2023/fitted-sizes.png)

*S is out of stock and 3XL is not available in this colour unfortunately.*

If you would be interested in one of the larger sizes please email the <a href="mailto:akademy-team@kde.org">Akademy Team</a> and we will arrange an alternative.

<div class="row">
  <div class="col-md-6">

### Thessaloniki: in person

In person attendees can pre-order T-shirts for collection in Thessaloniki for **15€** (paid when collecting in cash).

**Orders closed 1500 CEST Thursday 22nd June**

<!-- a href="https://conf.kde.org/event/5/registrations/22/" class="button">Pre-order your T-shirt</a -->

  </div>
  <div class="col-md-6">

### Online attendees

Online T-shirts cost **25€**, which includes worldwide delivery. To keep the price the same for everyone, [KDE e.V.](https://ev.kde.org) is subsidising the higher delivery cost for those outside Europe.

Orders for the online attendees are being handled directly by FreeWear, the same company who has been printing our Akademy T-shirts for years.

*The payment processing fees for KDE e.V. if you pay with credit-card are far cheaper than using PayPal.*

**Online orders are open till the 31st July and will start to be shipped from the 10th of August.**

<a href="https://www.freewear.org/Akademy/" class="button">Buy your T-shirt for online attendees</a>

  </div>
</div>

*FreeWear also carry a range of other <a href="https://www.freewear.org/kde">KDE & FLOSS merchandise</a> which you can order separately online<!-- or we will have a selection available in Barcelona-->.*

