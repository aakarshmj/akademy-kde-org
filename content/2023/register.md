---
title: Register
menu:
  "2023":
    parent: details
    weight: 1
---
Akademy is the annual world summit of [KDE](http://www.kde.org/), one of the largest Free Software communities in the world. It is a free, non-commercial event organized by the KDE Community. Akademy 2023 is a hybrid event that takes place in-person in Thessaloniki, ﻿Greece and o﻿nline from Saturday 15th - Friday 21st July, with a Welcome event on the Friday evening before. The [conference](/2023/program) is expected to draw hundreds of attendees from the global KDE Community to discuss and plan the future of the Community and its technology. Many participants from the broader Free and Open Source software community and users will also attend.

### Code of Conduct

Akademy 2023 has a [Code of Conduct](/2023/code-conduct) to ensure the event is an enjoyable experience for everyone. As a Community, we value and respect all people, regardless of gender identity, sexual orientation, race, ability, shape, size or preferred desktop environment. We will not tolerate vilification, abuse or harassment in any form.

### Covid-19 Mitigation

We will provide masks and tests for those who want them

If circumstances worsen we may also need to put in place additional measures that we will notify as early as possible.

**Akademy is free to attend however you need to register to reserve your space** 

*If you are new to KDE & Akademy, you will first need a [KDE identity Account](https://identity.kde.org/) to register*

<a href="https://conf.kde.org/event/5/registrations/18/" class="button">Register</a>

**After registering, to participate online join the Akademy space on KDE's Matrix server**

<a href="https://go.kde.org/matrix/#/#attend-akademy:kde.org" class="button">Interact & Watch the Talks</a>
