---
title: Day trip - Wednesday
menu:
  "2023":
    parent: program
    weight: 4
---

**Place:** Dion and Mount Olympus <br>
**Date:** Wednesday, 19 July 2023 <br>
**Time:** 0900 <br>

Our journey to Myth and History starts by visiting the ancient city and sanctuary of Dion, dedicated to the most important and powerful Olympic God , Zeus, the king of ancient Greek Dodecatheon and the ruler of the universe. The ancient people used to make sacrifices in the name of gods to please or appease them, among them the Great King of Macedonia,  Alexander. In the archaeological site of Dion we see a large temple dedicated to Zeus, and a series of temples dedicated to Demeter and Isis (the Egyptian god with the ability to create and destroy life), unique mosaic floors and ancient baths and ruins. Moving on, our experienced guide  will lead you to the  Dion Museum , where you will admire the ancient statues, relics and findings that were found in the site and listen to the myths and stories about everyday life of the people in ancient Dion

Our next stop is Stavros view point at Mount Olympus, where you enjoy breathtaking views of the so called “residence of the twelve Gods” and the endless surrounding area of the Olympus National Park.

We will then travel to the seaside village of Platamonas, where you can find places for food & drinks. The beach here is suitable for going in the water and swimming (at own risk)

<a href="https://conf.kde.org/event/5/registrations/21/" class="button">Register</a>

![Dion](/media/2023/dion_1.jpg)
![Dion](/media/2023/dion_2.jpg)
![Mount Olympus](/media/2023/olympus.jpg)


<strong>Plan for the day:</strong>

09.00 Departure from University of Macedonia main entrance. Please ensure you are here a bit early as we want to leave as sharp as possible

10.30 Arrival at Dion [Museum](https://archaeologicalmuseums.gr/en/museum/5df34af3deca5e2d79e8c1a7/archaeological-museum-of-dion) and [Archaeological Park](https://en.wikipedia.org/wiki/Archaeological_Park_of_Dion)
Entrance is a combined ticket for both, the normal entrance is 8€ [reductions for some categories of people are available](https://www.culture.gov.gr/en/service/SitePages/view.aspx?iID=2695) Entrance can be paid both with cash (not large notes) & card however it is advised in general to carry a small amount of cash on you for any other small expenses.

12.00 Departure to Mount Olympus

12:30 Arrival at Stavros highest shelter. View point for photos, optional walk in forest area

14:00 Departure for Platamonas sea village

14.45 Arrival in Platamonas village (beach area). Optional lunch at [local taverna](https://www.tripadvisor.com.gr/Restaurant_Review-g6158669-d3443531-Reviews-Psaropoula-Platamon_Pieria_Region_Central_Macedonia.html), there are also a number of beach bars in the area for coffee & drinks. Swimming is possible at the beach

16.30 Departure from Platamonas Village, with short stop for photos at nearby castle

19.00 Arrival in Thessaloniki _The above time table might slightly vary depending on traffic, departure time or any other normal delays._

<strong>Notes: </strong>
* You need to have [registered for the daytrip](https://conf.kde.org/event/5/registrations/21/) to attend and have your badge with you
* Museum entrance fee and any food/drinks you want need to be purchased yourself. _We want everyone to be able to take part in the Daytrip and money should not be the reason you miss out. If the entrance fee is too expensive for you due to being on a low income please speak to [Kenny D](https://go.kde.org/matrix/#/@kenny:kde.org) and we can cover that for you._
* Ensure you have water bottles and any snacks you might want
* Participants are required to wear comfortable shoes (preferably hiking shoes), light clothing/summer wear (an extra top may also come in handy along with a hat) 
* In case you wish to swim, please remember to bring your swim gear, a towel and sunscreen
* Phone signal/Internet is likely to be poor on Mount Olympus
