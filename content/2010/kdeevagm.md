---
title:       "KDE e.V. AGM"
date:        2010-03-16
changed:     2010-04-03
menu:
  "2010":
    parent: program
    weight: 4
---
The annual general assembly of KDE e.V. will take place on Monday, 5 July 2010.

