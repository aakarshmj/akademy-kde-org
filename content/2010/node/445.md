---
title:       "Toulouse University 4.0 - Fourth season great debriefing"
date:        2010-05-05
changed:     2016-11-02


---
<p><strong>Speakers:</strong> Benjamin Port, Rudy Commenge and Ludovic Deveaux</p>

<p>In the previous years Kevin Ottens bored you to death with his talks about student projects in Toulouse. He covered the setup in place, what worked or not and how you could do the same in an university near you.<br /><br />He gave some of the juicy details, but he doesn't know it all! This year, three of this year students are coming just for you and will deliver the naked truth. You'll get a chance to discover the cool stuff they did this year in very different KDE projects: Akonadi, Amarok and KPresenter.<br /><br />This joint talk will cover the following topics:<br />&nbsp;* A quick recap of the context of the IUP ISI and how the student projects<br />&nbsp; are implemented;<br />&nbsp;* This year technical work will be explained and demo'ed;<br />&nbsp;* The students will introspect on how they survived the experience, if they<br />&nbsp; liked it, if KDE developers bite.</p>
<h2>Benjamin Port</h2>
<p><img style="float: right;" src="/media/2010/speakers/benjamin_port.jpg" alt="" width="80" height="120" />Benjamin Port is a French student from the Toulouse university, France. His major is computing science. During his cursus he had the chance to contribute on KPresenter as an ervin's student. Now he continues to contribute to KOffice, currently he implements KPresenter animation as part of Google Summer of Code.</p>
<h2 style="clear: both;">Rudy Commenge</h2>
<p><img style="float: right;" src="/media/2010/speakers/rudy_commenge.jpg" alt="" width="156" height="200" />Rudy Commenge was born in 1988 and lives in Toulouse. Currently, he is a computer engineer in training at the Paul Sabatier University in Toulouse, he is a KDE contributor since 2009. Last year he joined the Akonadi team for the development of a resource for the communication with community site, such as a PHPBB forum. This project is still under in development</p>
<h2 style="clear: both;">Ludovic Deveaux</h2>
<p><img style="float: right;" src="/media/2010/speakers/ludovic_deveaux.jpg" alt="" width="153" height="200" />Ludovic Deveaux is an Amarok coder since 2009 (november). He has worked on this audio player for a university project under Kevin Ottens behalf. During near five months he was developing a new applet in the context view called "Upcoming Events". This applet allows Amarok users to know when their preferred artist will play in their city thanks to the LastFm API. Now, Ludovic is a trainee of an IT consulting company in Toulouse for five months. Next year, he will be in his last year of studies in Toulouse.</p>
