---
title:       "KStars showcase"
date:        2010-05-05
changed:     2016-11-02


---
<p><strong>Speaker:</strong> Akarsh Simha</p>
<p>The talk will be a quick demo of KStars for the non-astronomer. It will be an overview of KStars' target user base, its various use cases and a quick demo of some select features that we feel are unique to KStars and important to our users.</p>

<h2>Akarsh Simha</h2>
<p><img style="float: right;" src="/media/2010/speakers/kstar.jpg" alt="" />Akarsh 'kstar' Simha is a student of physics from India who has been contributing to KDE, KStars in particular, over the past 2 years. His first significant contributions to KStars happened as a summer of code student in 2008. After that, he maintained KStars for about an year, and is currently a GSoC mentor. He is also an amateur astronomer and<br />appreciates south Indian classical music.</p>
