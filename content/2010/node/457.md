---
title:       "Managing Localization: What you should and shouldn't do"
date:        2010-05-05
changed:     2016-11-02


---
<p><strong>Speaker:</strong> Park Shinjo</p>

<p>Although KDE is translated into lots of languages, not all languages are fully translated. End-user will use KDE SC in English, but as there isn't any barrier, you may start your own translation and share your work into upstream. Making translation is easy, but translating every essential parts of KDE Library/SC requires hard work. If somebody requested you about translating KDE, then another problem starts: collaboration and quality. Since you can't translate every parts of KDE alone, those two should be considered seriously. Building common phrasebook, team regulation, etc. will strengthen your role as new team coordinator. If you have enough manpower, time, motivation, then KDE will be fully localized into your language! :)<br /><br />However, things won't go as your expectations. You have to understand basic rules in localization, polish new translation until due dates for new version. Managing each team members couldn't go as your expectation, especially when team is not big enough. KDE is constantly evolving, new translation or major translation changes will appear in po file, sometimes hard to follow up. As a new team coordinator, understanting what is behind localization and how to communicate other team is required.<br /><br />As a KDE translator since 2006 and interim Korean translation team coordinator since late 2009, also translated KDE SC 4.x from zero to current status, I will give some notes to the newcomers of localization. The talk will cover my experience, surviving in KDE localization, some case studies about localization. Although this talk is targeted into new translation teams, topics won't be limited for them.</p>
<h2>Park Shinjo</h2>
<p><img style="float: right;" src="/media/2010/speakers/parkshinjo.jpg" alt="" width="200" height="182" />Park Shinjo is a translator on the KDE Korea team since 2006. His contribution includes lots of KDE 4.x translations. He is a third year student studying electrical engineering/computer science at KAIST, South Korea. When he's not dealing with boring books, he enjoys trainspotting around his school.</p>
