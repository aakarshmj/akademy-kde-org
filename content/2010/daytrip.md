---
title:       "Day Trip"
date:        2010-03-16
changed:     2016-11-02
menu:
  "2010":
    parent: social
    weight: 4
scssFiles:
- /scss/2010/images.scss
---
The Akademy day trip will take you to Merunlahti, Centre for Nature Tourism.  Merunlahti is located a short 15 minutes drive from Tampere, by the beautiful lake Höytämöjärvi.

<p>To get there, we will take buses from Tampere city centre, only 400 m south from Demola. Buses will leave at <strong>1 pm</strong> and return to city at 7 pm. Be there on time! The bus stop is located next to an old, small, wooden church (under renovation at the moment) in the central square (see <a href="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=vanha+kirkko+tampere&amp;sll=37.0625,-95.677068&amp;sspn=34.396866,79.013672&amp;ie=UTF8&amp;hq=&amp;hnear=Vanha+kirkko,+Aleksis+Kiven+Katu,+33210+Tampere,+Finland&amp;ll=61.500024,23.759952&amp;spn=0.005058,0.01929&amp;z=16">Google Maps</a>).</p>
<p>In Merunlahti we will have the opportunity to play some yard games, for example you can try out archery, play some forest golf, frisbee and darts - and naturally there's also a sauna for those who like to have a touch of pure Finnish culture! The nature around Merunlahti is really beautiful, so a walk along the forest tracks is also a good idea. Kayaks may be rented for EUR 8/h.  The sauna will be heated, and it fits 20 people. And of course swimming in the lake is a "must" thing to do! <strong>Bring your swimsuits and towels with you</strong>.</p>
<p>There will also be a BBQ (including a vegetarian option) and some drinks will be included on the day. It is totally OK to bring your own drinks with you, too.  Be sure to dress according to the weather, because all the activities will take place outside. There is only space inside for about 60 people.</p>
<p><img class="framed" style="display: block; margin-left: auto; margin-right: auto;" src="/media/2010/20100608_006.jpg" alt="" /></p>
<p>The day trip is sponsored by<br /><img src="/media/2010/collabora.med_.jpg" alt="Collabora Logo" width="240" height="79" /></p>
