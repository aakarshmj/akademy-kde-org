---
title:       "Accommodation"
date:        2010-03-16
changed:     2010-04-16
menu:
  "2010":
    parent: travel
    weight: 2
scssFiles:
- /scss/2010/images.scss
- /scss/tables.scss
- /scss/2010/table_sorted.scss
jsFiles:
- /js/2010/tablesort.js
---

<img class="img-fluid" src="/media/2010/hotel.page_.png" alt="" align="center" />

<p>On this page you can find information on hotels and hostels recommended by the organizers. Booking needs to be handled individually, unless stated otherwise.</p>

<p>Please be aware that we have organized special rates for most of the hotels and hostels for <strong>Akademy</strong>. These special rates only apply if you use a quota name, and if you book <strong>before the end of May/ early June</strong>. For details, please see list of hotels below.</p>
<p>All the hotels and hostels are in a walking distance from the University of Tampere and Finlayson area (<em>Demola</em>).</p>
<p>The following table will give you a short overview of available hotels and pricing. <br /><em>*Click on a column header to sort.<br />**Click on a hotel name to see the details.</em></p>
<table class="stretch sorted" onclick="sortColumn(event)">
<thead> 
<tr>
<td>Hotel</td>
<td>Single Room</td>
<td>Double Room</td>
<td>3+ Room (per person)<br /></td>
</tr>
</thead> 
<tbody>
<tr>
<td><a title="Go to Student House section" href="#studenthouse">Student House TOAS City</a></td>
<td>35 €</td>
<td>N/A</td>
<td>17,50 €</td>
</tr>
<tr>
<td><a title="Go to Dream Hotel section" href="#dream">Dream Hostel</a></td>
<td>N/A<br /></td>
<td>N/A</td>
<td>22,50 € - 33,50 €<br /></td>
</tr>
<tr>
<td><a title="Go to Sofia Hotel section" href="#sofia">Hostel Sofia</a></td>
<td>N/A<br /></td>
<td>72 €</td>
<td>27 €</td>
</tr>
<tr>
<td><a title="Go to Holiday Inn section" href="#holidayinn">Holiday Inn Tampere</a></td>
<td>80 €</td>
<td>95 €</td>
<td>N/A</td>
</tr>
<tr>
<td><a title="Go to Victoria section" href="#victoria">Hotel Victoria</a></td>
<td>89 €</td>
<td>99 €</td>
<td>N/A</td>
</tr>
<tr>
<td><a title="Go to Scandic section" href="#scandic">Scandic Tampere City</a></td>
<td>95 €</td>
<td>105 €</td>
<td>N/A</td>
</tr>
<tr>
<td><a title="Go to Omena Hotels section" href="#omena">Omena Hotels</a></td>
<td>55 €</td>
<td>55 €</td>
<td>18,33 €</td>
</tr>
<tr>
<td><a title="Go to Hotel Ville section" href="#ville">Hotel Ville</a></td>
<td>32 €</td>
<td>N/A</td>
<td>N/A</td>
</tr>
</tbody>
</table>
<h2 id="studenthouse"><a href="http://www.toas.fi/Tuomiokirkonkatu_19.1442.0.html" data-proofer-ignore>Student House TOAS City - Tuomiokirkonkatu 19</a></h2>
<p>All three floors of the Student House TOAS City are reserved for Akademy guests. This is THE place to be!<br /><br />A former factory building in the very city center, near the University of Tampere and Finlayson. All apartments and rooms are furnished and include linens and a locker. Studio apartments and big rooms to share with 2-3 guests. Common kitchens and showers. Some rooms have a minikitchen, shower and/or wc. Free gym and free hi-speed WLAN for the tentants.</p>
<p><img src="/media/2010/hotel.page_.toas_.png" alt="" align="right" /></p>
<h3>Price:</h3>
<ul>
<li>From 140 €/week/bed (17,50 €/night/bed) to 280 €/week/room (35 €/night/room) with own shower and wc.</li>
<li>Packets for accommodation are build in favor for those who are staying the whole Akademy week at Tampere.</li>
</ul>
<h3>How to book:</h3>
Book a room from <a href="http://www.coss.fi/en/akademy-student-house-toas" data-proofer-ignore>here</a>. Booking is handled via the local organizers.

<h2 id="dream"><a href="http://dreamhostel.fi/en/">Dream Hostel - Åkerlundinkatu 2</a></h2>
<p>Tampere Dream Hostel is a brand new hostel run by a young, traveller-minded family. The cost of a stay, regardless of which room, includes one set of linen. Additionally, inside the room for each guest there is a security locker, which is lockable with a one’s own padlock. Wi-Fi is available throughout the facilities for no charge. A kitchenette is freely available for guests for food preparation and preservation. The kitchenette features a refrigerator, freezer, oven, microwave, and all other fundamentals of a Finnish kitchen. Note that Finnish tap water is well drinkable (and it tastes good).</p>
<p><img src="/media/2010/hotel.page_.dream_.png" alt="" align="right" /></p>
<h3>Price:</h3>
Price per night, single bed in a shared room (price depends on the size of the room) - 22,50€-33,50€</li>

<h3>How to book:</h3>
<ul>
<li>Book a room by sending a mail to: info@dreamhostel.fi</li>
<li>Write in the "subject": Your Name, Akademy 2010</li>
<li>Reservations has to be made 31.5.2010 the latest.</li>
</ul>
<h2 id="sofia"><a href="http://www.hostelsofia.fi/" data-proofer-ignore>Hostel Sofia - Tuomiokirkonkatu 12 A</a></h2>
<p>Free wireless internet connection is provided in all rooms. Toilets and showers are along the corridor. On each floor there is a well-equipped self-catering kitchenette where you can make your own breakfast, supper and prepare other light snacks.</p>
<p><img src="/media/2010/hotel.page_.sofia_.png" alt="" align="right" /></p>
<h3>Price:</h3>
<ul>
<li>Twin room - 72 €/night </li>
<li>Bed in a shared triple room - 27 €/night </li>
</ul>
<p><em>Prices includes sheets and towels. Sleeping bags are not allowed.</em></p>
<h3>How to book:</h3>
<ul>
<li>Book a room by sending a mail to: hostelsofia@hostelsofia.fi</li>
<li>Quota name: "Akademy 2010"</li>
<li>Reservations has to be made 3.6.2010 the latest.</li>
</ul>
<h2 id="holidayinn"><a href="http://www.finland.holidayinn.com/hotellit/tampere/en_GB/tampere/" data-proofer-ignore>Holiday Inn Tampere - Yliopistonkatu 44</a></h2>
<p>Holiday Inn Tampere is a first class business hotel. All the rooms come with great beds, Pillow Menu, ADSL or WLAN broadband connection, hospitality tray and ironing equipment. Most of the double rooms have single beds.</p>
<p><img src="/media/2010/hotel.page_.hinn_.png" alt="" align="right" /></p>
<h3>Price:</h3>
<ul>
<li>Single room - 80€/night</li>
<li>Double room - 95€/night</li>
</ul>
<p><em>Breakfast is included.</em></p>
<h3>How to book:</h3>
<ul>
<li>Book a room by sending a mail to: tampere.holidayinn@restel.fi, remember to write the quota name.</li>
<li>Quota name: "Akademy 2010"</li>
<li>Reservations has to be made 5.6.2010 the latest.</li>
</ul>
<h2 id="victoria"><a href="http://www.hotellivictoria.fi/english/" data-proofer-ignore>Hotel Victoria - Itsenäisyydenkatu 1</a></h2>
<p>Comfortable hotel in the centre of Tampere. Fast and efficient broadband connection is also available at hotel rooms for an extra charge.</p>
<p><img src="/media/2010/hotel.page_.victoria.png" alt="" align="right" /></p>
<h3>Price:</h3>
<ul>
<li>Single room - 89 €/nigh</li>
<li>Double room - 99 €/night</li>
</ul>
<p><em>Price includes hearty breakfast.</em><br /> <em>The use of sauna and the swimming pool is also included in the room price.</em></p>
<h3>How to book:</h3>
<ul>
<li>Book a room by sending a mail to: sales@hotellivictoria.fi, remember to write the quota name.</li>
<li>Quota name: "Akademy 2010"</li>
<li>Reservations has to be made 3.6.2010 the latest.</li>
</ul>
<h2 id="scandic"><a href="http://www.scandichotels.com/en/Hotels/Countries/Finland/Tampere/Hotels/Scandic-Tampere-City/" data-proofer-ignore>Scandic Tampere City - Hämeenkatu 1</a></h2>
<p>The Scandic Tampere City hotel is located opposite the railway station.</p>
<p><img src="/media/2010/hotel.page_.scandic.png" alt="" align="right" /></p>
<h3>Price:</h3>
<ul>
<li>Single room - 95 €/night</li>
<li>Double room - 105 €/night (Queen and Twin beds)</li>
</ul>
<p><em>Breakfast is included and you can get fresh coffee 24 hours a day.</em> <em>Free wireless, sauna and fitness room.</em></p>
<h3>How to book:</h3>
<ul>
<li>Book a room at: <a href="http://www.scandichotels.com/">http://www.scandichotels.com/</a></li>
<li>Booking code: BAKA030710</li>
<li>Reservations has to be made 3.6.2010 the latest.</li>
</ul>
<h2 id="omena"><a href="http://www.omenahotels.com/frontpage">Omena Hotels - Hämeenkatu 7 and Hämeenkatu 28</a></h2>
<p>Omena Hotels is a new no-frills hotel chain, which offers high-class accommodation at a significantly lower rate than the general price level.</p>
<p><img src="/media/2010/hotel.page_.omena_.png" alt="" align="right" /></p>
<h3>Price:</h3>
Price: 50-55 €/night/room (the room fits max 3 persons)</li>

<p><em>The center of the room is a generously sized double bed. The comfortable sofa turns into a double bed in a matter of seconds, and each room is equipped with down quilts, synthetic quilts, bed linens, and towels for four persons. All our rooms have a toilet, shower, small refrigerator, microwave oven, electric kettle, hair dryer, tea, coffee and a dinner table for four.</em></p>
<h3>How to book:</h3>
<ul>
<li>Visitors must reserve and pay the rooms at <a href="http://www.omenahotels.com/hotel-tampere2">http://www.omenahotels.com/hotel-tampere2</a> and <a href="http://www.omenahotels.com/hotel-tampere">http://www.omenahotels.com/hotel-tampere</a></li>
<li>We do not have any quota.</li>
<li>You book your room on the Internet. Having completed your booking, you will receive a booking confirmation, which includes the room number and a personal door code that is valid throughout your stay. No further check-in or check-out procedures are needed.</li>
</ul>

<h2 id="ville"><a href="http://www.hotelliville.fi/english/index.html">Hotel Ville&nbsp;-&nbsp;Hatanpään valtatie 40</a></h2>
<p>Ville is a new kind of self-service hotel located near the centre of Tampere. The prices are very affordable and they offer a wide range of services. Room price includes high-speed internet connections in every room (WLAN). Standard equipment in each room: Television, microwave oven, mini fridge and electric kettle.</p>
<h3>Price:</h3>
<ul>
<li>We do not have any quota.</li>
<li>Rooms starting from 32 €/day</li>
</ul>
<h3>Booking:</h3>
There is no receptionist at the hotel, and so room reservations and changes to reservations are done over the internet. Our <a title="Book on-line now" href="https://www.hotelliville.fi/julkinen.php/default/index/lang/en_US">on-line booking</a> is quick, easy and safe. You will be given reservation code and password. By giving these codes in the hotel reservation (pub Oluthuone Ville) you will get the key code, which you can use to enter the hotel. So keep these codes SAFE!</li>
